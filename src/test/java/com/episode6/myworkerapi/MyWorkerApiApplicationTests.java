package com.episode6.myworkerapi;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.io.File;
import java.time.LocalDate;

// 유닛테스트

@SpringBootTest
class MyWorkerApiApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    void loadFileCheck() {
        // new File로 인해 임시 파일이 생겨 파일이 없어도 오류 발생하지 않음
        File file = new File(new File("").getAbsolutePath() + "/src/main/resource/static/me02.jpg");
        String test = "1";
    }

    @Test
    void Random() {
        // 겹치지 않게 해줌
        System.out.println(LocalDate.now());
    }
}
