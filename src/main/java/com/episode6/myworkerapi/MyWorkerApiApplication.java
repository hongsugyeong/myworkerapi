package com.episode6.myworkerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class MyWorkerApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyWorkerApiApplication.class, args);
    }

    // 고용시키기
    // Bean: 설정값 => 실행될 때부터 존재
    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

}
