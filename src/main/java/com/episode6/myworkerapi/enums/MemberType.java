package com.episode6.myworkerapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MemberType {

    // 지금 권한의 타입은 Sting이기 때문에 형식이 없 => 검사를 해주기 위해 규칙 추가 =>  enum으로 작성, 앞에 ROLE 붙여넣기
    ROLE_GENERAL("일반회원"),
    ROLE_BOSS("사업자"),
    ROLE_MANAGEMENT("관리자");

    private final String name;
}
