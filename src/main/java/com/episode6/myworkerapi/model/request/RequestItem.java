package com.episode6.myworkerapi.model.request;


import com.episode6.myworkerapi.entity.Request;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class RequestItem {
    private Long id;
    private Long BusinessId;
    private LocalDateTime dateRequest;


    private RequestItem(Builder builder) {
        this.id = builder.id;
        this.BusinessId = builder.BusinessId;
        this.dateRequest = builder.dateRequest;
    }


    public static class Builder implements CommonModelBuilder<RequestItem> {
        private final Long id;
        private final Long BusinessId;
        private final LocalDateTime dateRequest;


        public Builder(Request request) {
            this.id = request.getId();
            this.BusinessId = request.getBusiness().getId();
            this.dateRequest = request.getDateRequest();
        }
        @Override
        public RequestItem build() {
            return new RequestItem(this);
        }
    }

}
