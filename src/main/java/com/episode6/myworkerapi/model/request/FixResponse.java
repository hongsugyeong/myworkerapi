package com.episode6.myworkerapi.model.request;

import com.episode6.myworkerapi.entity.RequestFix;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FixResponse {
    private Long id;
    private Long businessId;
    private String businessName;
    private String ownerName;
    private String businessImgUrl;
    private String businessType;
    private String businessLocation;
    private String businessEmail;
    private String businessPhoneNumber;
    private String reallyLocation;
    private String etcMemo;

    private FixResponse(Builder builder) {
        this.id = builder.id;
        this.businessId = builder.businessId;
        this.businessName = builder.businessName;
        this.ownerName = builder.ownerName;
        this.businessImgUrl = builder.businessImgUrl;
        this.businessType = builder.businessType;
        this.businessLocation = builder.businessLocation;
        this.businessEmail = builder.businessEmail;
        this.businessPhoneNumber = builder.businessPhoneNumber;
        this.reallyLocation = builder.reallyLocation;
        this.etcMemo = builder.etcMemo;
    }

    public static class Builder implements CommonModelBuilder<FixResponse> {
        private final Long id;
        private final Long businessId;
        private final String businessName;
        private final String ownerName;
        private final String businessImgUrl;
        private final String businessType;
        private final String businessLocation;
        private final String businessEmail;
        private final String businessPhoneNumber;
        private final String reallyLocation;
        private final String etcMemo;


        public Builder(RequestFix fix) {
            this.id = fix.getId();
            this.businessId = fix.getBusiness().getId();
            this.businessName = fix.getBusinessName();
            this.ownerName = fix.getOwnerName();
            this.businessImgUrl = fix.getBusinessImgUrl();
            this.businessType = fix.getBusinessType();
            this.businessLocation = fix.getBusinessLocation();
            this.businessEmail = fix.getBusinessEmail();
            this.businessPhoneNumber = fix.getBusinessPhoneNumber();
            this.reallyLocation = fix.getReallyLocation();
            this.etcMemo = fix.getEtcMemo();
        }
        @Override
        public FixResponse build() {
            return new FixResponse(this);
        }
    }
}
