package com.episode6.myworkerapi.model.memberask;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberAskRequest {
    private String title;
    private String content;
    private String questionImgUrl;
}
