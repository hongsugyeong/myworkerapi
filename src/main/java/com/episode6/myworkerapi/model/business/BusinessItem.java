package com.episode6.myworkerapi.model.business;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BusinessItem {
    private Long id;
    private Long memberId;
    private String businessName;
    private String businessNumber;
    private String ownerName;
    private String businessImgUrl;
    private String businessType;
    private String businessEmail;
    private String businessPhoneNumber;
    private Boolean isActivity;
    private LocalDate dateJoinBusiness;
    private Boolean isApprovalBusiness;
    private String refuseReason;
    private String refuseFix;

    private BusinessItem(Builder builder) {
        this.id = builder.id;
        this.memberId = builder.memberId;
        this.businessName = builder.businessName;
        this.businessNumber = builder.businessNumber;
        this.ownerName = builder.ownerName;
        this.businessImgUrl = builder.businessImgUrl;
        this.businessType = builder.businessType;
        this.businessEmail = builder.businessEmail;
        this.businessPhoneNumber = builder.businessPhoneNumber;
        this.isActivity = builder.isActivity;
        this.dateJoinBusiness = builder.dateJoinBusiness;
        this.isApprovalBusiness = builder.isApprovalBusiness;
        this.refuseReason = builder.refuseReason;
        this.refuseFix = builder.refuseFix;
    }

    public static class Builder implements CommonModelBuilder<BusinessItem> {

        private final Long id;
        private final Long memberId;
        private final String businessName;
        private final String businessNumber;
        private final String ownerName;
        private final String businessImgUrl;
        private final String businessType;
        private final String businessEmail;
        private final String businessPhoneNumber;
        private final Boolean isActivity;
        private final LocalDate dateJoinBusiness;
        private final Boolean isApprovalBusiness;
        private final String refuseReason;
        private final String refuseFix;


        public Builder(Business business) {
            this.id = business.getId();
            this.memberId = business.getMember().getId();
            this.businessName = business.getBusinessName();
            this.businessNumber = business.getBusinessNumber();
            this.ownerName = business.getOwnerName();
            this.businessImgUrl = business.getBusinessImgUrl();
            this.businessType = business.getBusinessType();
            this.businessEmail = business.getBusinessEmail();
            this.businessPhoneNumber = business.getBusinessPhoneNumber();
            this.isActivity = business.getIsActivity();
            this.dateJoinBusiness = business.getDateJoinBusiness();
            this.isApprovalBusiness = business.getIsApprovalBusiness();
            this.refuseReason = business.getRefuseReason();
            this.refuseFix = business.getRefuseFix();
        }
        @Override
        public BusinessItem build() {
            return new BusinessItem(this);
        }
    }
}
