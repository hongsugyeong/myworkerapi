package com.episode6.myworkerapi.model.board;

import com.episode6.myworkerapi.entity.Board;
import com.episode6.myworkerapi.enums.Category;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;


@Getter
@Setter
public class BoardResponse {
    private Long id;
    private Category category;
    private String title;
    private String content;
    private String boardImgUrl;
    private LocalDateTime dateBoard;

    private Long memberId;

    private BoardResponse(Builder builder) {
        this.id = builder.id;
        this.category = builder.category;
        this.title = builder.title;
        this.content = builder.content;
        this.boardImgUrl = builder.boardImgUrl;
        this.dateBoard = builder.dateBoard;
        this.memberId = builder.memberId;
    }

    public static class Builder implements CommonModelBuilder<BoardResponse> {
        private final Long id;
        private final Category category;
        private final String title;
        private final String content;
        private final String boardImgUrl;
        private final LocalDateTime dateBoard;
        private final Long memberId;

        public Builder(Board board) {
            this.id = board.getId();
            this.category = board.getCategory();
            this.title = board.getTitle();
            this.content = board.getContent();
            this.boardImgUrl = board.getBoardImgUrl();
            this.dateBoard = board.getDateBoard();
            this.memberId = board.getMember().getId();

        }
        @Override
        public BoardResponse build() {
            return new BoardResponse(this);
        }
    }
}
