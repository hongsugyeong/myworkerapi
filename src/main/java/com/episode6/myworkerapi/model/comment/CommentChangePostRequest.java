package com.episode6.myworkerapi.model.comment;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommentChangePostRequest {
    private String content;
}
