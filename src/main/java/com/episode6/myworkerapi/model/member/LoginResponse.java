package com.episode6.myworkerapi.model.member;

import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.*;

// 로그인 기능 구현 시작

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LoginResponse {
    // 멤버 아이디와 비밀번호를 토큰으로 바꿈
    private String token;
    private String name;

    public LoginResponse(Builder builder) {
        this.name = builder.name;
        this.token = builder.token;
    }

    public static class Builder implements CommonModelBuilder<LoginResponse> {

        private final String name;
        private final String token;

        // String 이용해서 받아오기
        public Builder(String token, String name) {
            this.name = name;
            this.token = token;
        }

        @Override
        public LoginResponse build() {
            return new LoginResponse(this);
        }
    }
}
