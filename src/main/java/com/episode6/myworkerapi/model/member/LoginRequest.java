package com.episode6.myworkerapi.model.member;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

// 로그인 할 경우 받아야 할 값

@Getter
@Setter
public class LoginRequest {

    @NotNull
    @Length(min = 5, max = 20)
    private String username;

    @NotNull
    @Length(min = 8, max = 20)
    private String password;
}
