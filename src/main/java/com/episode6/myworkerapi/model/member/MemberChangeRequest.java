package com.episode6.myworkerapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberChangeRequest {
    private String name;
    private String phoneNumber;
    private String address;
}
