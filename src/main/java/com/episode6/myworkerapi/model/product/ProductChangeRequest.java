package com.episode6.myworkerapi.model.product;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductChangeRequest {
    private String productName;
    private Short minQuantity;
}
