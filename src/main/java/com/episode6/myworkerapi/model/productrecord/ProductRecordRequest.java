package com.episode6.myworkerapi.model.productrecord;

import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Product;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class ProductRecordRequest {
    private Short nowQuantity;
    private LocalDateTime dateProductRecord;
}
