package com.episode6.myworkerapi.model.businessmember;

import com.episode6.myworkerapi.enums.Position;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BusinessMemberRequest {
    private Position position;
    private String etc;
}
