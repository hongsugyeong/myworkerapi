package com.episode6.myworkerapi.model.report;



import com.episode6.myworkerapi.entity.ReportBoard;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReportBoardItem {
    private Long boardId;
    private Long memberId;
    private String content;
    private LocalDateTime dateReport;


    private ReportBoardItem(Builder builder) {
        this.boardId = builder.boardId;
        this.memberId = builder.memberId;
        this.content = builder.content;
        this.dateReport = builder.dateReport;
    }

    public static class Builder implements CommonModelBuilder<ReportBoardItem> {
        private final Long boardId;
        private final Long memberId;
        private final String content;
        private final LocalDateTime dateReport;

        public Builder(ReportBoard reportBoard) {
            this.boardId = reportBoard.getId();
            this.memberId = reportBoard.getMember().getId();
            this.content = reportBoard.getContent();
            this.dateReport = LocalDateTime.now();
        }
        @Override
        public ReportBoardItem build() {
            return new ReportBoardItem(this);
        }
    }
}
