package com.episode6.myworkerapi.model.manual;


import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Manual;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ManualResponse {
    private Long id;
    private Long businessMemberId;
    private LocalDate dateManual;
    private String title;
    private String content;
    private String manualImgUrl;


    private ManualResponse(Builder builder) {
        this.id = builder.id;
        this.businessMemberId = builder.businessMemberId;
        this.dateManual = builder.dateManual;
        this.title = builder.title;
        this.content = builder.content;
        this.manualImgUrl = builder.manualImgUrl;

    }
    public static class Builder implements CommonModelBuilder<ManualResponse> {
        private final Long id;
        private final Long businessMemberId;
        private final LocalDate dateManual;
        private final String title;
        private final String content;
        private final String manualImgUrl;

        public Builder (Manual manual) {
            this.id = manual.getId();
            this.businessMemberId = manual.getBusinessMember().getId();
            this.dateManual = manual.getDateManual();
            this.title = manual.getTitle();
            this.content = manual.getContent();
            this.manualImgUrl = manual.getManualImgUrl();
        }
        @Override
        public ManualResponse build() {
            return new ManualResponse(this);
        }
    }
}
