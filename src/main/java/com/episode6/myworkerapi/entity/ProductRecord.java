package com.episode6.myworkerapi.entity;


import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.productrecord.ProductRecordRequest;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor
public class ProductRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "productId")
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @JoinColumn(name = "businessmemberId")
    @ManyToOne(fetch = FetchType.LAZY)
    private BusinessMember businessMember;

    @Column(nullable = false)
    private Short nowQuantity;

    @Column(nullable = false)
    private LocalDateTime dateProductRecord;

    private ProductRecord(Builder builder) {
        this.product = builder.product;
        this.businessMember = builder.businessMember;
        this.nowQuantity = builder.nowQuantity;
        this.dateProductRecord = builder.dateProductRecord;
    }

    public static class Builder implements CommonModelBuilder<ProductRecord> {
        private final Product product;
        private final BusinessMember businessMember;
        private final Short nowQuantity;
        private final LocalDateTime dateProductRecord;

        public Builder(Product product, BusinessMember businessMember, ProductRecordRequest request) {
            this.product = product;
            this.businessMember = businessMember;
            this.nowQuantity = request.getNowQuantity();
            this.dateProductRecord = LocalDateTime.now();
        }

        @Override
        public ProductRecord build() {
            return new ProductRecord(this);
        }
    }
}
