package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.model.product.ProductChangeRequest;
import com.episode6.myworkerapi.model.product.ProductItem;
import com.episode6.myworkerapi.model.product.ProductRequest;
import com.episode6.myworkerapi.model.product.ProductResponse;
import com.episode6.myworkerapi.service.*;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/product")
public class ProductController {
    private final BusinessService businessService;
    private final BusinessMemberService businessMemberService;
    private final ProductService productService;
    private final DeleteService deleteService;

    @PostMapping("/join/business-member-id/{businessMemberId}")
    @Operation(summary = "상품 등록")
    public CommonResult setProduct(@PathVariable long businessMemberId, @RequestBody ProductRequest request) {
        BusinessMember businessMember = businessMemberService.getBusinessMemberData(businessMemberId);
        productService.setProduct(businessMember, request);
        return ResponseService.getSuccessResult();
    }
    @GetMapping("/all/{pageNum}")
    @Operation(summary = "재고 상품 전체 리스트로 보기 관리자")
    public ListResult<ProductItem> getProductsAdmin(@PathVariable int pageNum) {
        return ResponseService.getListResult(productService.getProductsAdmin(pageNum),true);
    }

    @GetMapping("/all/business-id/{businessId}/{pageNum}")
    @Operation(summary = "사업장 재고 상품 리스트로 보기")
    public ListResult<ProductItem> getProducts(@PathVariable long businessId, @PathVariable int pageNum) {
        Business business = businessService.getBusinessData(businessId);
        return ResponseService.getListResult(productService.getProducts(business, pageNum),true);
    }

    @GetMapping("/detail/product-id/{productId}")
    @Operation(summary = "재고 상품 상세보기")
    public SingleResult<ProductResponse> getProduct(@PathVariable long productId) {
        return ResponseService.getSingleResult(productService.getProduct(productId));
    }

    @PutMapping("/change/product-id/{productId}")
    @Operation(summary = "재고 상품 이름 최소수량 수정")
    public CommonResult putProduct(@PathVariable long productId, @RequestBody ProductChangeRequest request) {
        productService.putProduct(productId, request);
        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/product-id/{productId}")
    @Operation(summary = "재고 상품 삭제하기")
    public CommonResult delProduct(@PathVariable long productId) {
        deleteService.delProduct(productId);
        return ResponseService.getSuccessResult();
    }


}
