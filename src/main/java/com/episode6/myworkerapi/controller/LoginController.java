package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.enums.MemberType;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.model.member.LoginRequest;
import com.episode6.myworkerapi.model.member.LoginResponse;
import com.episode6.myworkerapi.service.LoginService;
import com.episode6.myworkerapi.service.ResponseService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// 권한 주기
// 우리는 웹 관리자와 사장님 웹, 알바생과 사장님 앱 총 4개가 있어서 4번 해줘야 된다네요
    // 관리자- 웹 | 사장님- 웹 | 알바생- 앱 | 사장님- 앱
@RestController
@RequestMapping("/v1/login")
@RequiredArgsConstructor
public class LoginController {
    private final LoginService loginService;

    // 웹으로 로그인 할 경우- 관리자
    @PostMapping("/web/admin")
    // @Valid: 객체의 제약 조건을 검증
    public SingleResult<LoginResponse> doLoginWebAdmin(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberType.ROLE_MANAGEMENT, loginRequest, "WEB"));
    }

    // 웹으로 로그인 할 경우- 사장님
    @PostMapping("/web/boss")
    public SingleResult<LoginResponse> doLoginWebBoss(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberType.ROLE_BOSS, loginRequest, "WEB"));
    }

    // 앱으로 로그인 할 경우- 알바생
    @PostMapping("/app/general")
    public SingleResult<LoginResponse> doLoginAppGeneral(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberType.ROLE_GENERAL, loginRequest, "APP"));
    }

    // 앱으로 로그인 할 경우- 사장님
    @PostMapping("/app/boss")
    public SingleResult<LoginResponse> doLoginAppBoss(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberType.ROLE_BOSS, loginRequest, "APP"));
    }
}
