package com.episode6.myworkerapi.controller;


import com.episode6.myworkerapi.entity.Comment;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.report.ReportCommentItem;
import com.episode6.myworkerapi.model.report.ReportRequest;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.service.CommentService;
import com.episode6.myworkerapi.service.MemberService;
import com.episode6.myworkerapi.service.ReportCommentService;
import com.episode6.myworkerapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/report-comment")
public class ReportCommentController {
    private final MemberService memberService;
    private final CommentService commentService;
    private final ReportCommentService reportCommentService;

    @PostMapping("join/member-id/{memberId}/comment-id/{commentId}")
    @Operation(summary = "댓글 신고 중복신고 불가능")
    public CommonResult setReportComment(@RequestBody ReportRequest request, @PathVariable long memberId, @PathVariable long commentId) throws Exception {
        Member member = memberService.getMemberData(memberId);
        Comment comment = commentService.getCommentData(commentId);

        reportCommentService.setReportComment(member,comment,request);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("all/{pageNum}")
    @Operation(summary = "댓글 신고 최신순 페이징")
    public ListResult<ReportCommentItem> setReportCommentPage(@PathVariable int pageNum) {

        return ResponseService.getListResult(reportCommentService.setReportCommentPage(pageNum),true);
    }

    @DeleteMapping("del/comment-id/{commentId}")
    @Operation(summary = "댓글 신고 삭제")
    public CommonResult delReportComment(@PathVariable long commentId) {

        reportCommentService.delReportComment(commentId);

        return ResponseService.getSuccessResult();
    }
}
