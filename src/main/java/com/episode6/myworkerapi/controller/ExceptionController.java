package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.exception.CMemberPasswordException;
import com.episode6.myworkerapi.model.common.CommonResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/exception")
public class ExceptionController {

    // 로그인 안 했을 때 접근하는 경우 거부
    @GetMapping("/access-denied")
    public CommonResult accessDeniedException() {
        throw new CMemberPasswordException();
    }

    // 로그인 했을 때 권한 없는 사람이 침범하는 경우
    @GetMapping("/entry-point")
    public CommonResult entryPointException() {
        throw new CMemberPasswordException();
    }

}
