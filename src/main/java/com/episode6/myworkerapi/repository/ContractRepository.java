package com.episode6.myworkerapi.repository;

import com.episode6.myworkerapi.entity.Contract;
import com.episode6.myworkerapi.model.contract.ContractResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContractRepository extends JpaRepository<Contract, Long> {

    Page<Contract> findAllByOrderByIdDesc(Pageable pageable);

    Contract findAllByBusinessMemberId(long id);

    Page<Contract> findByBusinessMember_BusinessId(Pageable pageable, long id);

}
