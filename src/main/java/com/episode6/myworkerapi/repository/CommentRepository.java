package com.episode6.myworkerapi.repository;

import com.episode6.myworkerapi.entity.Board;
import com.episode6.myworkerapi.entity.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {

    /**
     * (페이징) 게시물 최신순 정렬
     */
    Page<Comment> findAllByOrderByIdDesc(Pageable pageable);

    /**
     * 댓글 엔티티에서 게시물 아이디 찾기
     */
    List<Comment> findByBoard_Id(long id);
}
