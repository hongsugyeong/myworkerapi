package com.episode6.myworkerapi.repository;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.Transition;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransitionRepository extends JpaRepository<Transition, Long> {
    Page<Transition> findAllByOrderByIdDesc(Pageable pageable);

    Page<Transition> findAllByBusinessMember_BusinessOrderByIdDesc(Business business, Pageable pageable);
}
