package com.episode6.myworkerapi.repository;


import com.episode6.myworkerapi.entity.Comment;
import com.episode6.myworkerapi.entity.ReportComment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReportCommentRepository extends JpaRepository<ReportComment, Long> {

    /**
     * 댓글 신고 최신순
     */
    Page<ReportComment> findAllByOrderByIdDesc(Pageable pageable);

    /**
     * 리포트코멘트에서 코멘트아이디 찾기 (D)
     */
    List<ReportComment> findByComment_Id(long id);
}
