package com.episode6.myworkerapi.exception;

public class CMemberUsernameCheckException extends RuntimeException{
    public CMemberUsernameCheckException(String msg, Throwable t) {
        super(msg, t);
    }
    public CMemberUsernameCheckException(String msg) {
        super(msg);
    }
    public CMemberUsernameCheckException() {
        super();
    }
}
