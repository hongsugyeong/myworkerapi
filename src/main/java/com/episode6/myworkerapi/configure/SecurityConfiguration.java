package com.episode6.myworkerapi.configure;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor
public class SecurityConfiguration {
    private final JwtTokenProvider jwtTokenProvider;
    private final CustomAuthenticationEntryPoint entryPoint;
    private final CustomAccessDeniedHandler accessDenied;

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration config = new CorsConfiguration();

        config.setAllowCredentials(true);
        config.setAllowedOrigins(List.of("*"));
        // 맵핑 종류 여섯가지
        // OPTIONS: api 호출할 때 한 요청당 두번씩 감 => preflight의 메서드가 OPTIONS
        // Pre-flight: 미리 전송 (실제 요청이 전송하기에 안전한지 확인)
        // cors의 요청 방식 중 하나로 아래와 같은 options 방식을 사용했을 경우 쓰임?
        config.setAllowedMethods(List.of("GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"));
        config.setAllowedHeaders(List.of("*"));
        // 노출된 헤더는 모두 허용
        config.setExposedHeaders(List.of("*"));

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        // 모든 맵핑 주소에 대해 밑의 설정을 주겠다
        source.registerCorsConfiguration("/**", config);
        return source;
    }

    /*
    flutter에서 header에 토큰 넣는 방법
    앱에서는 토큰이 비휘발성 메모리에 저장됨
    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!; <= 얘 추가하기

    vuejs에서 axios에 header 넣는 방법
    https://velog.io/@ch9eri/Axios-headers-Authorization
     */

    @Bean
    // 설정을 체인처럼 엮기
    // 설정값 갖고 검사하기
    protected SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        // csrf 끔
            // api 구조를 갖고 있기 때문에 csrf 사용할 필요 없음
        // csrf: 세션 파일을 인증되지 않은 다른 주소로 넘겼기 때문에 발생한 오류임
        // 세션이랑 쿠키 방식을 사용하면 활성화하는데 지금 토큰 방식 사용하고 있어서 비활성화 해줌
        http.csrf(AbstractHttpConfigurer::disable)
                .formLogin(AbstractHttpConfigurer::disable)
                .httpBasic(AbstractHttpConfigurer::disable)
                // cors 설정을 시큐리티 체인으로 묶기
                // 프론터가 다른 출처를 통해 들어오기 때문에 cors 활성화
                .cors(corsConfig -> corsConfig.configurationSource(corsConfigurationSource()))
                // csrf랑 엮어서 생각하기
                .sessionManagement((sessionManagement) ->
                        // STATELESS 값 없음
                        sessionManagement.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                // authorize: 인증: 나라는 걸 확인받는 과정, 인가: 인증 요청을 허가하는 과정
                // 아래의 과정은 인가 과정임 (시큐리티는 인가를 해주는 애)
                // 데이터에 대해서 인가를 해준다
                // 맵핑 풀기
                .authorizeHttpRequests((authorizeRequests) ->
                        authorizeRequests
                                // v3: 버전 3
                                .requestMatchers("/v3/**", "/swagger-ui/**").permitAll()
                                // permitAll: 먼저 뚫어준다
                                // ** => 모든 게 들어오는 자리
                                .requestMatchers(HttpMethod.GET, "/exception/**").permitAll()
                                .requestMatchers("/v1/member/login/**").permitAll()
                                .requestMatchers("/v1/member/join/**").permitAll()
                                // 그 외의 공간 아님
                                // 권한을 갖게 할 역할을 넣어주기 => 권한 설정하기
                                .requestMatchers("/v1/auth-test/test-admin").hasAnyRole("MEMBER" ,"ADMIN")
                                // 그 외의 공간
                                .anyRequest().hasRole("ADMIN")
                );
        // 핸들러에게 오류 넘기기
        http.exceptionHandling(handler -> handler.accessDeniedHandler(accessDenied));
        http.exceptionHandling(handler -> handler.authenticationEntryPoint(entryPoint));
        // 수행하기 전 토큰프로바이더에게 회원정보 자리에 문제가 있는????
        // .class: 필터의 자리
        http.addFilterBefore(new JwtTokenFilter(jwtTokenProvider), UsernamePasswordAuthenticationFilter.class);

        // 넘겨주기
        // http는 리턴 값을 갖고 있지 않은데 어떻게 return을 했을까 => 엎어쓰기 해서 변수 필요없음
        return http.build();
    }

}
