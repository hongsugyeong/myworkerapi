package com.episode6.myworkerapi.configure;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;

// 기능 묶음
@Component
// implements을 통해 상세기준 구현(프로젝트마다 보안 방침 달라서 하나하나 구현해야)
public class CustomAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    // http: 데이터, servlet: 서빙
    //
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {

        // /exception/access-denied: 주소
        response.sendRedirect("/exception/access-denied");
    }
}
