package com.episode6.myworkerapi.configure;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import java.io.IOException;

@Component
@RequiredArgsConstructor
// 필터 종류 여러 개 => 어떤 종류의 필터든 bean(*시작할 때의 설정값)에 등록
public class JwtTokenFilter extends GenericFilterBean {

    private final JwtTokenProvider jwTokenProvider;

    // 고객 -> 서버, 서버 -> 고객
    // 필터 단계 설정 후 ~~
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String tokenFull = jwTokenProvider.resolveToken((HttpServletRequest) request);
        String token = "";
        // resolve 값이 없거나 Bearer로 시작하지 않는다면
        if (tokenFull == null || !tokenFull.startsWith("Bearer ")) {
            chain.doFilter(request, response);
            // return 타입이 void: return을 만나는 순간 끝남
            return;
        }

        // split: 띄어쓰기
        // trim: 앞뒤 공백 제거
        token = tokenFull.split(" ")[1].trim();
        // 제공자에게 토큰 검사 부탁
        if (!jwTokenProvider.validateToken(token)) {
            chain.doFilter(request, response);
            return;
        }

        // 토큰을 갖고 일회용 출입증 발급 받기
        Authentication authentication = jwTokenProvider.getAuthentication(token);
        // context: 환경, Holder: 고정
        // 환경에 일회용 출입증 고정시키기
        // 서비스와 컨트롤러 등에서 고정된 것 확인 후 ~
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
    }
}
