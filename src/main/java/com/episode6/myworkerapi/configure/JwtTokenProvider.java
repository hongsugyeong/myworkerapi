package com.episode6.myworkerapi.configure;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import java.util.Base64;
import java.util.Date;

@Component
@RequiredArgsConstructor
// Jwt토큰 제공, 생성, 검사
public class JwtTokenProvider {
    private final UserDetailsService userDetailsService;
    @Value("${spring.jwt.secret}")
    private String secretKey;

    // 구조를 먼저 만들기
    @PostConstruct
    protected void init() {
        // Base64: 예전 암호화 기법 (*단방향)
        // secretKey를 암호화 시키기?
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
    }

    // 토큰 생성
    // claim: 주장하다 => 보안 토큰 주장 => 나라고 주장
    public String createToken(String username, String role, String type) {
        Claims claims = Jwts.claims().setSubject(username);
        // role: 역할
        claims.put("role", role);
        // localDate가 아닌 Date인 이유: 외국 사람들 고려 => 기준점 하나로
        Date now = new Date();
        // 토큰 유효시간
        // 1000 밀리세컨드 = 1초
        // 기본으로 10시간 유효하게 설정해줌 => 아침에 출근해서 로그인하고 점심먹고 퇴근하면 대략 10시간
        // 앱용 토큰 같은 경우 유효시간 1년으로 설정 => 앱에서 아침마다 로그인 하지 않도록
        // 유지 후 파기
        // 쿠키는 시간 동일하게 맞춰야 됨
        // 앱은 1년동안 유지
        long tokenValiMillisecond = 1000L * 60 * 60 * 10;
        if (type.equals("APP")) tokenValiMillisecond = 1000L * 60 * 60 * 24 * 365;
        // 토큰 생성해서 리턴
        // jwt 사이트 참고하기
        // 유효시간 넣어줌 => 생성요청한 시간 ~ 현재 + 위에서 설정된 유효초 만큼
        // Jwts를 찍어내는 클래스 그런데 쌓는 방식인,,
        return Jwts.builder()
                .setClaims(claims) // 나다
                .setIssuedAt(now) // ~ 때에 발급됨
                .setExpiration(new Date(now.getTime() + tokenValiMillisecond)) // ~ 때에 만료됨
                .signWith(SignatureAlgorithm.HS256, secretKey) // 사인 => secretKey
                .compact();
    }


    // 토큰 분석하여 인증정보 가져오기
    // loadUserByUsername 호출 => UserDetails 받음
    // principal: 중요한
    // Authentication: 일회용 출입증
    public Authentication getAuthentication(String token) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(getUsername(token));
        // principal(중요한 정보 넣기) credentials(신원 인증할 때 사용하는데 userDetail이 이미 있어서 빈칸으로 두기) authorities
        // 인증정보 만들기
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    // 토큰 파싱하여 username 가져오기
    // 토큰 생성시 username은 subject에 넣은 것 확인하기
    // jwt 사이트 보면서 코드 이해하기
    public String getUsername(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
    }

    // resolve: 반환된 성공의 결과물
        // api 호출뿐만 아니라 함수 사이에서도 사용되는 개념
    // vue resolve
        // promise와 resolve
        // 미래의 다른 서버에 데이터를 요청할 때 응답이 약속이 되어 있는 상태
    // resolveToken: 성공한 토큰
    // 헤더에서 토큰값만 가져오기 (*요청이 성공해야지만 가져올 수 있음)
    public String resolveToken(HttpServletRequest request) {
        // rest api - header 인증 방식에서 Bearer를 언제 사용하는지 보기
        return request.getHeader(HttpHeaders.AUTHORIZATION);
    }

    // 유효시간을 검사하여 유효시간이 지났으면 false를 줌
    // 토큰이 정상적인지 검사
    // try catch 쓴 이유: 유지보수
        // 문제가 생기면 던져야 되는데 일단 던지고 보기 때문에 try catch 사용
    public boolean validateToken(String jwtToken) {
        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(jwtToken);
            return !claims.getBody().getExpiration().before(new Date());
        } catch (Exception e) {
            return false;
        }
    }
}
