package com.episode6.myworkerapi.service;


import com.episode6.myworkerapi.entity.Board;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.enums.Category;
import com.episode6.myworkerapi.model.board.BoardChangePostRequest;
import com.episode6.myworkerapi.model.board.BoardItem;
import com.episode6.myworkerapi.model.board.BoardRequest;
import com.episode6.myworkerapi.model.board.BoardResponse;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.repository.BoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardService {
    private final BoardRepository boardRepository;

    /**
     * 아이디값 가져오기
     */
    public Board getBoardData(long id) {
        return boardRepository.findById(id).orElseThrow();
    }

    /**
     * 게시글 등록
     */
    public void setBoard(BoardRequest boardRequest, Member member) {
        boardRepository.save(new Board.Builder(boardRequest,member).build());
    }

    /**
     *
     * 게시물 10개당 1페이지 최신순
     * @return 페이징
     */
    public ListResult<BoardItem> getBoardPage(int pageNum){
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Board> boards = boardRepository.findAllByOrderByIdDesc(pageRequest);

        List<BoardItem> boardItems = new LinkedList<>();
        for (Board board : boards.getContent()) boardItems.add(new BoardItem.Builder(board).build());

        return ListConvertService.settingResult(boardItems
                ,boards.getTotalElements()
                ,boards.getTotalPages()
                ,boards.getPageable().getPageNumber());
    }

    /**
     * 게시물 상세보기
     */
    public BoardResponse getBoard(long id) {
        Board board = boardRepository.findById(id).orElseThrow();
        return new BoardResponse.Builder(board).build();
    }

    /**
     * 게시물 내용 수정
     */
    public void putBoard(long id, BoardChangePostRequest request) {
        Board board = boardRepository.findById(id).orElseThrow();
        board.putBoard(request);
        boardRepository.save(board);
    }

    /**
     * 카테고리 별 페이징 (INTEGRATION, 통합)
     */
    public ListResult<BoardItem> getIntegrationPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Board> boardPage = boardRepository.findAllByCategoryOrderByIdDesc(pageRequest,Category.INTEGRATION);

        List<BoardItem> boardItemList = new LinkedList<>();
        for (Board board : boardPage.getContent()) boardItemList.add(new BoardItem.Builder(board).build());

        return ListConvertService.settingResult(
                boardItemList
                ,boardPage.getTotalElements()
                ,boardPage.getTotalPages()
                ,boardPage.getPageable().getPageNumber());
    }

    /**
     * 카테고리 별 페이징 (BOSS, 사장님)
     * */
    public ListResult<BoardItem> getBossPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Board> boardPage = boardRepository.findAllByCategoryOrderByIdDesc(pageRequest, Category.BOSS);

        List<BoardItem> boardItemList = new LinkedList<>();
        for (Board board : boardPage.getContent()) boardItemList.add(new BoardItem.Builder(board).build());

        return ListConvertService.settingResult(
                boardItemList
                ,boardPage.getTotalElements()
                ,boardPage.getTotalPages()
                ,boardPage.getPageable().getPageNumber());
    }

    /**
     * 카테고리 별 페이징 (JOB, 알바생)
     */
    public ListResult<BoardItem> getJobPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Board> boardPage = boardRepository.findAllByCategoryOrderByIdDesc(pageRequest, Category.JOB);

        List<BoardItem> boardItemList = new LinkedList<>();
        for (Board board : boardPage.getContent()) boardItemList.add(new BoardItem.Builder(board).build());

        return ListConvertService.settingResult(
                boardItemList
                ,boardPage.getTotalElements()
                ,boardPage.getTotalPages()
                ,boardPage.getPageable().getPageNumber()
        );
    }
}
