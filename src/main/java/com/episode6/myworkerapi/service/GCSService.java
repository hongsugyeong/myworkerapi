package com.episode6.myworkerapi.service;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
// Stream: byte로 조금씩 줌
import java.io.InputStream;

// 최종은 멀티파트파일로 만들기
@Service
@RequiredArgsConstructor
public class GCSService {

    // application.yaml 에서 경로
    @Value("${spring.cloud.gcp.storage.bucket}")
    private String bucketName;

    @Value("${spring.cloud.key}")
    private String SECRET_KEY;

    public void uploadObject(MultipartFile multipartFile) throws IOException {
        InputStream keyFile = ResourceUtils.getURL("classpath" + SECRET_KEY).openStream();

        // storage 객체 생성
        Storage storage = StorageOptions.newBuilder()
                .setCredentials(GoogleCredentials.fromStream(keyFile))
                .build()
                .getService();

        BlobInfo blobInfo = BlobInfo.newBuilder(bucketName, multipartFile.getOriginalFilename())
                .setContentType(multipartFile.getContentType()).build();

        Blob blob = storage.create(blobInfo, multipartFile.getInputStream());
    }

}
