package com.episode6.myworkerapi.service;


import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.RequestFix;
import com.episode6.myworkerapi.enums.ResultCode;
import com.episode6.myworkerapi.model.request.FixItem;
import com.episode6.myworkerapi.model.request.FixRequest;
import com.episode6.myworkerapi.model.request.FixResponse;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.repository.BusinessRepository;
import com.episode6.myworkerapi.repository.RequestFixRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RequestFixService {
    private final RequestFixRepository requestFixRepository;
    private final BusinessRepository businessRepository;

    /**
     * 사장님이 수정 요청  , 수정내용 자동 메모
     */
    public void setRequestFix(Business business, FixRequest request) {
        requestFixRepository.save(new RequestFix.Builder(request, business).build());
    }


    /**
     * 수정 요청 수락 , 수정 내용 변경 후 삭제 (프론트에서 기본값 넣는 것 해야함)
     */
    public void putRequestFix(long id) {
        RequestFix requestFix = requestFixRepository.findById(id).orElseThrow();
        Business business = businessRepository.findById(requestFix.getBusiness().getId()).orElseThrow();

        business.PutBusinessFix(requestFix);
        businessRepository.save(business);

        requestFixRepository.deleteById(id);
    }

    /**
     * 수정 요청 최신순 페이징
     */
    public ListResult<FixItem> getFixPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);

        Page<RequestFix> requestFixPage = requestFixRepository.findAllByOrderByIdDesc(pageRequest);

        List<FixItem> fixItemList = new LinkedList<>();
        for (RequestFix fix : requestFixPage.getContent()) {
            fixItemList.add(new FixItem.Builder(fix).build());
        }
        return ListConvertService.settingResult(
                fixItemList
                ,requestFixPage.getTotalElements()
                ,requestFixPage.getTotalPages()
                ,requestFixPage.getPageable().getPageNumber()
        );
    }

    /**
     * 수정 요청 단수보기 / 사장님 수정할 입력 폼, 관리자 자세히 보기
     */
    public FixResponse getRequestFix(long id) {
        RequestFix requestFix = requestFixRepository.findById(id).orElseThrow();
        return new FixResponse.Builder(requestFix).build();
    }
}
