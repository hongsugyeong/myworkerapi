package com.episode6.myworkerapi.service;


import com.episode6.myworkerapi.entity.Board;
import com.episode6.myworkerapi.entity.Comment;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.model.comment.CommentChangePostRequest;
import com.episode6.myworkerapi.model.comment.CommentItem;
import com.episode6.myworkerapi.model.comment.CommentRequest;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.repository.CommentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CommentService {
    private final CommentRepository commentRepository;

    /**
     * 코멘트 아이디값 가져오기
     * */
    public Comment getCommentData(long id) {
        return commentRepository.findById(id).orElseThrow();
    }

    /**
     * 댓글 등록
     */
    public void setComment(CommentRequest request, Member member, Board board) {
        commentRepository.save(new Comment.Builder(request,member,board).build());
    }

    /**
     * 댓글 최신순 보기 10개당 1페이지
     */
    public ListResult<CommentItem> getCommentPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Comment> comments = commentRepository.findAllByOrderByIdDesc(pageRequest);

        List<CommentItem> commentItems = new LinkedList<>();

        for (Comment comment : comments.getContent()) commentItems.add(new CommentItem.Builder(comment).build());

        return ListConvertService.settingResult(
                commentItems
                ,comments.getTotalElements()
                ,comments.getTotalPages()
                ,comments.getPageable().getPageNumber()
        );
    }
    /**
     * 댓글 상세보기
     */
    public CommentItem getComment(long id) {
        Comment comment = commentRepository.findById(id).orElseThrow();
        return new CommentItem.Builder(comment).build();
    }

    /**
     * 댓글 내용 수정
     */
    public void putComment(CommentChangePostRequest request , long id) {
        Comment comment = commentRepository.findById(id).orElseThrow();
        comment.putComment(request);

        commentRepository.save(comment);
    }

}


