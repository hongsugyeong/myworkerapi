package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.model.business.*;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.repository.BusinessRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BusinessService {
    private final BusinessRepository businessRepository;

    public Business getBusinessData(long id) {
        return businessRepository.findById(id).orElseThrow();
    }

    /**
     * 사업장 등록
     */
    public void setBusiness(BusinessRequest request, Member member) {
        businessRepository.save(new Business.Builder(request,member).build());
    }

    /**
     * 사업장 최신순 페이징
     */

    public ListResult<BusinessItem> getBusinessPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);

        Page<Business> businessPage = businessRepository.findByOrderByIdDesc(pageRequest);

        List<BusinessItem> result = new LinkedList<>();
        for (Business business : businessPage) {
            result.add(new BusinessItem.Builder(business).build());
        }
        return ListConvertService.settingResult(
                result
                ,businessPage.getTotalElements()
                ,businessPage.getTotalPages()
                ,businessPage.getPageable().getPageNumber()
        );
    }

    /**
     * 사업장 상세보기
     */
    public BusinessResponse getBusiness(long id) {
        Business originData = businessRepository.findById(id).orElseThrow();
        return new BusinessResponse.Builder(originData).build();

    }

    /**
     * 관리자용 사업장 수정
     */
    public void putBusiness(long id, BusinessChangeRequest request) {
        Business business = businessRepository.findById(id).orElseThrow();
        business.putBusinessChangeRequest(request);
        businessRepository.save(business);
    }

    /**
     * 사장님 사업장 실근무지 수정
     */
    public void putBusinessLocation(long id, BusinessLocationRequest request) {
        Business business = businessRepository.findById(id).orElseThrow();
        business.putReallyLocation(request);
        businessRepository.save(business);
    }
}