package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.exception.CMemberPasswordException;
import com.episode6.myworkerapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class CustomUserDetailService implements UserDetailsService {
    private final MemberRepository memberRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        // 방침 주기
        Member member = memberRepository.findByUsername(username).orElseThrow(CMemberPasswordException::new);

        return null;
    }
}
