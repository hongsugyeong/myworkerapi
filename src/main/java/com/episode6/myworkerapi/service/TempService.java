package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.lib.CommonFile;
import com.episode6.myworkerapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

// 이미지 리사이즈

@Service
@RequiredArgsConstructor
public class TempService {
    private final MemberRepository memberRepository;

    int AFTER_SIZE = 1000;

    public static void setMemberByFile(MultipartFile multipartFile) throws IOException {
        File file = CommonFile.multipartFile(multipartFile);
        BufferedImage inputImage = ImageIO.read(file);
    }

    private File imageResize(BufferedImage bufferedImage) throws IOException {
        int widthImage = bufferedImage.getWidth();
        int heightImage = bufferedImage.getHeight();
        int newSize = AFTER_SIZE;

        int resultWidth = widthImage >= heightImage ? newSize : (newSize * widthImage) / heightImage;
        int resultHeight = widthImage >= heightImage ? (newSize * heightImage) / widthImage : newSize;

        return graphicsResize(resultWidth, resultHeight, bufferedImage);
    }

    private File graphicsResize(int widthImage, int heightImage, BufferedImage bufferedImage) throws IOException {
        Image resizedImage = bufferedImage.getScaledInstance(widthImage, heightImage, Image.SCALE_SMOOTH); // 이미지 사이즈 수정: 가로 크기, 세로 크기, 변환 방법(SCALE_SMOOTH: 이미지 부드럽게)
        BufferedImage newImage = new BufferedImage(widthImage, heightImage, bufferedImage.TYPE_INT_RGB); // 결과물 옮길 이미지 생성 (width와 height 가진)
        Graphics graphics = newImage.getGraphics(); // 새로 생성한 이미지 불러오기
        graphics.drawImage(resizedImage, 0, 0, null); // 새 이미지에 원본 이미지 그리기
        graphics.dispose(); // 연결 끊기
        File newFile = new File("src/main/java/image/me02.png"); // 리사이즈할 파일 경로
        ImageIO.write(newImage, "png", newFile);

        return newFile;
    }
}
