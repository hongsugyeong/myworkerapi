package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.configure.JwtTokenProvider;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.enums.MemberType;
import com.episode6.myworkerapi.exception.CMemberPasswordException;
import com.episode6.myworkerapi.model.member.LoginRequest;
import com.episode6.myworkerapi.model.member.LoginResponse;
import com.episode6.myworkerapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoginService {
    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;

    // 로그인 타입은 WEB 또는 APP (WEB일 경우 토큰 유효시간 10시간, APP은 1년)
    public LoginResponse doLogin(MemberType memberType, LoginRequest loginRequest, String loginType) {

        // 회원 정보 없습니다 던지기
        Member member = memberRepository.findByUsername(loginRequest.getUsername()).orElseThrow(CMemberPasswordException::new);

        // 일반 회원이 관리자용으로 로그인 시도할 경우 회원 정보가 없습니다 메시지 던지기
        if (!member.getMemberType().equals(memberType)) throw new CMemberPasswordException();
        // 비밀번호가 일치하지 않습니다 메시지 던지기
        // matches: 로그인 시도의 비밀번호와 멤버의 비밀번호가 서로 맞는지 확인
        if (!passwordEncoder.matches(loginRequest.getPassword(), member.getPassword())) throw new CMemberPasswordException();

        // 토큰에 값 넣어주기?
        String token = jwtTokenProvider.createToken(String.valueOf(member.getUsername()), member.getMemberType().toString(), loginType);

        // 모델에서 만들었던 빌더패턴 사용
        return new LoginResponse.Builder(token, member.getName()).build();
    }
}
