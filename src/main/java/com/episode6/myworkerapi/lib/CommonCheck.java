package com.episode6.myworkerapi.lib;

public class CommonCheck {
    public static boolean checkUsername (String username) {
        String pattern = "^[a-zA-Z]{1}[a-zA-Z0-9]{4,19}$";
        return username.matches(pattern);
    }
}
