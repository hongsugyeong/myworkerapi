package com.episode6.myworkerapi.lib;


import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

// 정규화

public class CommonFile {
    public static File multipartFile (MultipartFile multipartFile) throws IOException {
        File convFile = new File(System.getProperty("java.io.tmdir") + "/" + multipartFile.getOriginalFilename());
        multipartFile.transferTo(convFile);

        return convFile;
    }
}
